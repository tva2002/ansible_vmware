#!/bin/bash
INPUT=data.csv
OLDIFS=$IFS
IFS=','
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read ip netmask descript name
do
    echo "IP : $ip"
    echo "NETMASK : $netmask"
    echo "DESCRIPT : $descript"
    echo "NAME : $name"
    ansible-playbook  netbox_ua.yml --extra-vars "{"ip_address": "$ip","netmask": "$netmask","ip_description": "$descript","vm_name": "$name"}"
done < $INPUT
IFS=$OLDIFS